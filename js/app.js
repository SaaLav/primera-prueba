/*JQuery.validator.addMethod("campo-texto", function(value, element)
{
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "solo puedes ingresar texto");
*/

$(function(){
  $("#formulario").validate({
    rules: {
      nombre: {
        required: true,
        email: true,
        rut: true,
        fna: true,
        Telefono: true

      },
      messages: {
        nombre:{
          required:"Ingresa tu nombre"},
          email:{
            required:"ingresa tu email"
          },
          rut:{
          required:"ingresa tu rut"}
        
      }

    }
  });
});


$('.carousel').carousel({
  interval: 3000
});

//MODAL//

var imagenes = ['Wifi','Pexel','Oso','Luna','Maya','Chocolate'];
var gal = document.getElementById('gal');
for(ima of imagenes)
{
  gal.innerHTML += `<div class="card">
  <a href="#" data-toggle="modal" data-target="#exampleModal${ima}">
    <img src="css/imagenes/rescatados/${ima}.jpg" class="card-img-top" alt="">
  </a> 
  </div>
<!-- Modal -->
<div class="modal fade" id="exampleModal${ima}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true">
 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
  <div class="modal-dialog modal-dialog-centered" role="document">
    <img src="css/imagenes/rescatados/${ima}.jpg" class="img-fluid rounded" alt="">
  </div>
</div>
  `
}